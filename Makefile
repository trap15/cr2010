SOO		 = sooparse
CR2010OBJS	 = src/base.o src/CRBoard.o src/CRBot.o src/CRDuck.o src/CROil.o src/CRSponge.o src/CRWall.o
CR2010SRCS	 = src/base.c src/CRBoard.c src/CRBot.c src/CRDuck.c src/CROil.c src/CRSponge.c src/CRWall.c
CR2010HEADS	 = include/base.h include/CRBoard.h include/CRBot.h include/CRDuck.h include/CROil.h include/CRSponge.h include/CRWall.h
CBC_TARGET	 = 1
GCC_TARGET	 = 2
BUILD_TARGET	 = $(GCC_TARGET)
ifeq ($(BUILD_TARGET),$(CBC_TARGET))
OBJECTS		 = main.o
SOURCES		 = main.c
else
OBJECTS		 = main.o $(CR2010OBJS)
SOURCES		 = main.c $(CR2010SRCS)
HEADERS		 = config.h $(CR2010HEADS)
endif
OUTPUT		 = cr2010test
DEFS		 = -DBUILD_TARGET=$(BUILD_TARGET)
OPTIMIZE	 = -O2
LIBS		 = 
LIBDIRS		 = 
INCLUDES	 = -Iinclude -I.
CFLAGS		 = -g -std=c99 $(INCLUDES) $(OPTIMIZE)
LDFLAGS		 = $(LIBDIRS) $(LIBS)
CC		 = gcc
RM		 = rm
CLEANED		 = $(OUTPUT) $(OBJECTS) $(HEADERS) $(SOURCES) soo.h
#MAKELIST	 = $(SOURCES) $(OBJECTS)

all: $(HEADERS) $(MAKELIST) $(OUTPUT)
%.h: %.hoo
	$(SOO) $< $@
%.c: %.coo
	$(SOO) $< $@
%.o: %.c
	$(CC) $(CFLAGS) $(DEFS) -c -o $@ $<
$(OUTPUT): $(OBJECTS)
	$(CC) $(LDFLAGS) -o $(OUTPUT) $(OBJECTS)
clean:
	$(RM) -f $(CLEANED)
run:
	./$(OUTPUT)
release: clean
