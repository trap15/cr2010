/*****************************************************************************
 *  Config                                                                   *
 *  Part of Challenger Robotics Library                                      *
 *- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*
 *  Copyright (C)2010          trap15 (Alex Marshall) <trap15@raidenii.net>  *
 *  All rights reserved.                                                     *
 *- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -*
 *                    http://github.com/SquidMan/cr2010                      *
 *****************************************************************************/

#ifndef CONFIG_H_
#define CONFIG_H_

#define CBC_TARGET			(1)
#define GCC_TARGET			(2)

#ifndef BUILD_TARGET
#define BUILD_TARGET			(CBC_TARGET)
#endif

#define COMPETITION			(1)
#define LIGHT_PORT			(4)

#define LEFT_MOTOR			(0)
#define RIGHT_MOTOR			(1)
#define TURN_SPEEDL			(251)
#define TURN_SPEEDR			(250)
#define MOVE_SPEEDL			(251)
#define MOVE_SPEEDR			(250)

#define INCH2TICKL(x)			((long)(98.639f * ((float)(x))))
#define INCH2TICKR(x)			((long)(98.639f * ((float)(x))))

#define TURNTICKSL_LEFT_45		(- 258)
#define TURNTICKSR_LEFT_45		(  258)
#define TURNTICKSL_LEFT_90		(- 515)
#define TURNTICKSR_LEFT_90		(  515)
#define TURNTICKSL_LEFT_135		(- 772)
#define TURNTICKSR_LEFT_135		(  772)
#define TURNTICKSL_LEFT_180		(-1030)
#define TURNTICKSR_LEFT_180		( 1030)
#define TURNTICKSL_RIGHT_45		(  258)
#define TURNTICKSR_RIGHT_45		(- 258)
#define TURNTICKSL_RIGHT_90		(  515)
#define TURNTICKSR_RIGHT_90		(- 515)
#define TURNTICKSL_RIGHT_135		(  772)
#define TURNTICKSR_RIGHT_135		(- 772)
#define TURNTICKSL_RIGHT_180		( 1030)
#define TURNTICKSR_RIGHT_180		(-1030)

#define PIPE_T				(1.5f)
#define HEIGHT_AVOIDANCE		(PIPE_T)
#define WIDTH_AVOIDANCE			(PIPE_T)

#define BOTHIGH				(16.5f)
#define BOTWIDE				(11.0f)

#define ARM_MOTOR			(3)
#define ARM_SPEED			(150)
#define ARM_CALIBRATION_GRANULATION	(10)
#define ARM_CALIBRATION_PORT		(14)
#define ARM_OFFSET			(-10)
#define ARM_BASKET_POSITION		(-445 + ARM_OFFSET)

#define CLAW_MOTOR			(2)
#define CLAW_SPEED			(500)
#define CLAW_CALIBRATION_GRANULATION	(10)
#define CLAW_CALIBRATION_PORT		(15)
#define CLAW_DOWN_POSITION_MIN		(-1480)
#define CLAW_DOWN_POSITION_MAX		(-1600)
#define CLAW_BASKET_POSITION		(-900)
#define CLAW_UP_POSITION		(0)

#define GRABBER_SERVO			(3)
#define GRABBER_OPEN			(675)
#define GRABBER_OPEN2			(725)
#define GRABBER_CLOSED			(300)

#define DUCK_SPOTS			(5)
#define DUCKS_LOCATIONS			{ { - 580, -1240 }, \
					  { - 620, -1240 }, \
					  { - 680, -1240 }, \
					  { - 726, -1240 }, \
					  { - 762, -1350 }, \
					}
#define DROP_LOCATIONS			{ { -   0, -1200 }, \
					  { -  35, -1190 }, \
					  { -  90, - 975 }, \
					  { - 130, - 870 }, \
                                          { - 150, - 870 }, \
					}
#define DROP_MOVES			{ { 0.00f,-2.00f }, \
					  { 0.00f,-2.00f }, \
					  { 0.00f,-1.50f }, \
					  { 0.00f, 2.00f }, \
					  { 0.00f, 4.00f }, \
					}
#define GRAB_OPENS			{ 0, \
					  0, \
					  0, \
					  1, \
					  1, \
					}

#endif /* CONFIG_H_ */
